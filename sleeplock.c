// Sleeping locks

#include "types.h"
#include "defs.h"
#include "param.h"
#include "x86.h"
#include "memlayout.h"
#include "mmu.h"
#include "proc.h"
#include "spinlock.h"
#include "sleeplock.h"

void
initsleeplock(struct sleeplock *lk, char *name)
{
  initlock(&lk->lk, "sleep lock");
  lk->name = name;
  lk->locked = 0;
  lk->pid = 0;
}

void
acquiresleep(struct sleeplock *lk)
{
  acquire(&lk->lk);
  while (lk->locked) {
    sleep(lk, &lk->lk);
  }
  lk->locked = 1;
  lk->pid = myproc()->pid;
  release(&lk->lk);
}

void
releasesleep(struct sleeplock *lk)
{
  acquire(&lk->lk);
  lk->locked = 0;
  lk->pid = 0;
  wakeup(lk);
  release(&lk->lk);
}

int
holdingsleep(struct sleeplock *lk)
{
  int r;
  
  acquire(&lk->lk);
  r = lk->locked && (lk->pid == myproc()->pid);
  release(&lk->lk);
  return r;
}

// LAB4 semaphore Queue handling private functions

void 
enter_queue(struct semaphore *sem , struct proc* new_proc){
  new_proc->next_sem = 0;
  if(sem->head_queue == 0){
    sem->head_queue = new_proc;
    sem->tail_queue = new_proc;
  }else{
    sem->head_queue->next_sem = new_proc;
    sem->head_queue = new_proc;
  }
}

struct proc*
exit_queue(struct semaphore *sem){
  struct proc* exited_proc = sem->tail_queue;
  if(sem->tail_queue->next_sem == 0){
    sem->head_queue = 0;
    sem->tail_queue = 0;
  }else
    sem->tail_queue = sem->tail_queue->next_sem;
  
  return exited_proc; 
}

// LAB4 semaphore main functions

void
init_semaphore(struct semaphore *sem , int _value , int index)
{
  initlock(&sem->lk, "semaphore");
  sem->sem_id = index;
  sem->value = _value;
  sem->head_queue = 0;
  sem->tail_queue = 0;
}

void
acquire_semaphore(struct semaphore *sem)
{
  acquire(&sem->lk);
  struct proc* p = myproc();
  sem->value--;
  if(sem->value < 0){
    enter_queue(sem , p);
    cprintf("Process %d has slept on semaphore %d\n" , p->pid , sem->sem_id);
    sleep(p , &sem->lk);
  }
  release(&sem->lk);
}

void
release_semaphore(struct semaphore *sem)
{
  acquire(&sem->lk);
  sem->value++;
  if(sem->value <= 0){
    struct proc* p = exit_queue(sem);
    cprintf("Process %d has waken up on semaphore %d\n" , p->pid , sem->sem_id);
    wakeup(p);
  }
  release(&sem->lk);
}