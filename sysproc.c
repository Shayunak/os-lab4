#include "types.h"
#include "x86.h"
#include "defs.h"
#include "date.h"
#include "param.h"
#include "CondVar.h"
#include "memlayout.h"
#include "mmu.h"
#include "proc.h"

int
sys_fork(void)
{
  return fork();
}

int
sys_exit(void)
{
  exit();
  return 0;  // not reached
}

int
sys_wait(void)
{
  return wait();
}

int
sys_kill(void)
{
  int pid;

  if(argint(0, &pid) < 0)
    return -1;
  return kill(pid);
}

int
sys_getpid(void)
{
  return myproc()->pid;
}

int
sys_sbrk(void)
{
  int addr;
  int n;

  if(argint(0, &n) < 0)
    return -1;
  addr = myproc()->sz;
  if(growproc(n) < 0)
    return -1;
  return addr;
}

int
sys_sleep(void)
{
  int n;
  uint ticks0;

  if(argint(0, &n) < 0)
    return -1;
  acquire(&tickslock);
  ticks0 = ticks;
  while(ticks - ticks0 < n){
    if(myproc()->killed){
      release(&tickslock);
      return -1;
    }
    sleep(&ticks, &tickslock);
  }
  release(&tickslock);
  return 0;
}

// return how many clock tick interrupts have occurred
// since start.
int
sys_uptime(void)
{
  uint xticks;

  acquire(&tickslock);
  xticks = ticks;
  release(&tickslock);
  return xticks;
}

// LAB4 system calls

// LAB4 Part 1 Semaphore System Call Handlers and Test Handlers
int 
sys_semaphore_initialize(void){

  int i, val, m;
  struct semaphore* sem;

  if(argint(0, &i) < 0 || argint(1, &val) < 0 || argint(2, &m) < 0)
    return -1;  

  sem = get_sem(i);

  init_semaphore(sem, val - m , i);

  cprintf("Semaphore %d with value %d and initial process %d initialized\n" , i , val , m);
  
  return i;
}

int
sys_semaphore_acquire(void){

  int i;
  struct semaphore* sem;

  if(argint(0, &i) < 0)
    return -1;

  sem = get_sem(i);

  acquire_semaphore(sem);

  cprintf("Semaphore %d acquired by process %d\n" , i , myproc()->pid);

  return i;
}

int 
sys_semaphore_release(void){

  int i;
  struct semaphore* sem;

  if(argint(0, &i) < 0)
    return -1;

  sem = get_sem(i);

  release_semaphore(sem);

  cprintf("Semaphore %d released by process %d\n" , i , myproc()->pid);
  
  return i;
}
// LAB4 Producer/Consumer Problem system calls for semaphore test
int
sys_consume(void){
  int data_consumed = get_data();
  cprintf("Data %d has been consumed by process %d\n" , data_consumed , myproc()->pid);
  return data_consumed;
}

int
sys_produce(void){
  int data_produced;
  if(argint(0, &data_produced) < 0)
    return -1;
  
  put_data(data_produced);
  cprintf("Data %d has been produced by process %d\n" , data_produced , myproc()->pid);
  return 0;
}

// LAB4 Part 2 Conditional Variable System Call Handlers

int 
sys_cv_init(void){
  struct condvar* cond_var;
  if(argptr(0, (void*)&cond_var, sizeof(struct condvar)) < 0)
    return -1;
  init_spinlock_user(&cond_var->lk);
  cond_var->locked = 0;
  return 0;
}


int 
sys_cv_wait(void){
  struct condvar* cond_var;
  if(argptr(0, (void*)&cond_var, sizeof(struct condvar)) < 0)
    return -1;
  
  // acquire_spinlock_user(&cond_var->lk);
  if(cond_var->locked){
    sleep1(cond_var, &cond_var->lk);
  }
  cond_var->locked = 1;
  // release_spinlock_user(&cond_var->lk);

  return 0;
}

int 
sys_cv_signal(void){
  struct condvar* cond_var;
  if(argptr(0, (void*)&cond_var, sizeof(struct condvar) ) < 0)
    return -1;

  // acquire_spinlock_user(&cond_var->lk);
  cond_var->locked = 0;
  wakeup(cond_var);
  // release_spinlock_user(&cond_var->lk);

  return 0;
}

int shared_counter;
int read_count;
struct spinlock_user mutex;
struct spinlock_user rw_mutex; 

int 
sys_reader(void){
  acquire_spinlock_user(&mutex);
  read_count++;

  if(read_count == 1)
    acquire_spinlock_user(&rw_mutex);

  release_spinlock_user(&mutex);

  cprintf("reader with pid %d read %d\n", myproc()->pid, shared_counter);

  acquire_spinlock_user(&mutex);
  read_count--;

  if(read_count == 0)
    release_spinlock_user(&rw_mutex);

  release_spinlock_user(&mutex);

  return 0;
}

int 
sys_writer(void){
  acquire_spinlock_user(&rw_mutex);

  shared_counter++;

  cprintf("writer with pid %d wrote %d\n", myproc()->pid , shared_counter);
  release_spinlock_user(&rw_mutex);  

  return 0;
}

int 
sys_init_reader_writer(void){
  init_spinlock_user(&mutex);
  init_spinlock_user(&rw_mutex);
  read_count = 0;
  shared_counter = 0;
  return 0;
}
