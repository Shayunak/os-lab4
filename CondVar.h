// LAB4 Spinlock_user and Condition Variable implementation kernel
#include "types.h"

struct spinlock_user {
    uint locked;
};


struct condvar{
    uint locked;
    struct spinlock_user lk;
};
