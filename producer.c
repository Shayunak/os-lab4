#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"

// int main(int argc, char *argv[]){
//     semaphore_acquire(0);
//     printf(1 , "Producer doing shit.\n");
//     sleep(1000);
//     semaphore_release(0);
//     exit();
// }

#define BUFF_SIZE 3 // In order to change it,Also change it in param.h(Kernel Definition)
#define MUTEX 0
#define EMPTY 1
#define FULL 2
#define DATA 69

int main(int argc, char *argv[]){
    while(1){
        sleep(500); //Producing
        semaphore_acquire(EMPTY);
        semaphore_acquire(MUTEX);
        produce(DATA);
        semaphore_release(MUTEX);
        semaphore_release(FULL);
    }
    exit();
}