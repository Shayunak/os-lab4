// Long-term locks for processes
struct sleeplock {
  uint locked;       // Is the lock held?
  struct spinlock lk; // spinlock protecting this sleep lock
  
  // For debugging:
  char *name;        // Name of lock.
  int pid;           // Process holding lock
};

//Semaphore Structure
//LAB4

struct semaphore {
  int sem_id;
  int value; // number of processes int the wait queue
  struct proc* head_queue;
  struct proc* tail_queue;
  struct spinlock lk; // // spinlock protecting this semaphore
};
