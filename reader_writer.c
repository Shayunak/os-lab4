#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"

int main(){
    uint pid1, pid2;
    init_reader_writer();
    pid1 = fork();
    if(pid1 == 0){
        //reader1
        int i;
        for(i = 0; i < 3; i++){
            sleep(200);
            reader();
        }
    }
    else{
        pid2 = fork();
        if(pid2 == 0){
        //reader2
            int i;
            for(i = 0; i < 5; i++){
                sleep(200);
                reader();
            }
        }else{
        //writer
            int i;
            for(i = 0; i < 10; i++){
                sleep(100);
                writer();
            }
            wait();
            wait();
        }
    }
    exit();
}