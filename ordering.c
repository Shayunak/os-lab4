#include "types.h"
#include "user.h"

int main(){
    struct condvar cond;
    cv_init(&cond);
    cond.locked = 1;
    int pid = fork();
    if(pid < 0){
        printf(1, "Error forking first child.\n");
        exit();
    } else if(pid == 0){
        cv_wait(&cond);
        printf(1, "Child 1 executing.\n"); 
        exit();
    } else {
        pid = fork();
        if(pid < 0){
            printf(1, "Error forking second child.\n");
        exit();
        } else if(pid == 0){
            printf(1, "Child 2 executing.\n");
            cv_signal(&cond);
            exit();
        } else {
            int i;
            for(i = 0; i < 2; i++){
                wait();
            }
        }
    }
    
    exit();
}