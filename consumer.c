#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"

// int main(int argc, char *argv[]){
//     semaphore_initialize(0 , 2 , 0);
//     semaphore_acquire(0);
//     printf(1 , "Consumer doing shit.\n");
//     sleep(2000);
//     semaphore_release(0);
//     exit();
// }

#define BUFF_SIZE 3 // In order to change it,Also change it in param.h(Kernel Definition)
#define MUTEX 0
#define EMPTY 1
#define FULL 2

int main(int argc, char *argv[]){
    semaphore_initialize(MUTEX , 1 , 0);
    semaphore_initialize(EMPTY , BUFF_SIZE , 0);
    semaphore_initialize(FULL , 0 , 0);
    while(1){
        semaphore_acquire(FULL);
        semaphore_acquire(MUTEX);
        consume();
        semaphore_release(MUTEX);
        semaphore_release(EMPTY);
        sleep(1000); //Consuming
    }
    exit();
}